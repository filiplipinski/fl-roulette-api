const rp = require("request-promise");

const apiUrl = "https://roulette-api.nowakowski-arkadiusz.com";

// Helpers to make the code shorter

const post = (path, hashname = "", body = {}) =>
  rp({
    method: "POST",
    uri: apiUrl + path,
    body,
    json: true,
    headers: { Authorization: hashname },
  });

const get = (path, hashname = "") =>
  rp({
    method: "GET",
    uri: apiUrl + path,
    json: true,
    headers: { Authorization: hashname },
  });

module.exports = { get, post };
