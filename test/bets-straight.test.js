const { get, post } = require("../utils/http");

describe("Pojedyncze numery", () => {
  test.each([1])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/1", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([2])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/2", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([3])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/3", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([4])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/4", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([5])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/5", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([6])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/6", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([7])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/7", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([8])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/8", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([9])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/9", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([10])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/10", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([11])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/11", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([12])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/12", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([13])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/13", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([14])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/14", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([15])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/15", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([16])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/16", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([17])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/17", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([18])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/18", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([19])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/19", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([20])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/20", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([21])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/21", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([22])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/22", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([23])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/23", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([24])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/24", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([25])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/25", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([26])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/26", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([27])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/27", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([28])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/28", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([29])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/29", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([30])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/30", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([31])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/31", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([32])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/32", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([33])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/33", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([34])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/34", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([35])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/35", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });

  test.each([36])("Zaklad: pojedynczy numer. Numer = %d", (number) => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/straight/36", hashname, { chips: 100 });
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(3600));
  });
});
