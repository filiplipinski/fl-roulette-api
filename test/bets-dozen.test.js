const { get, post } = require("../utils/http");

describe("Grupy po tuzin numerow", () => {
  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].forEach((number) => {
    test(`Zakład na tuzin numerow, zwraca 3x zaklad, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/dozen/1", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(300));
    });
  });

  [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24].forEach((number) => {
    test(`Zakład na tuzin numerow, zwraca 3x zaklad, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/dozen/2", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(300));
    });
  });

  [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36].forEach((number) => {
    test(`Zakład na tuzin numerow, zwraca 3x zaklad, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/dozen/3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(300));
    });
  });
});
