const { get, post } = require("../utils/http");

describe("Sasiadujace 2 numery", () => {
  test.each([0, 1])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/0-1", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([1, 2])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/1-2", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([1, 4])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/1-4", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([2, 5])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/2-5", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([3, 6])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/3-6", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([2, 3])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/2-3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([4, 7])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/4-7", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([5, 8])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/5-8", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([6, 9])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/6-9", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([3, 4])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/3-4", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([7, 10])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/7-10", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([8, 11])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/8-11", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([9, 12])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/9-12", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([4, 5])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/4-5", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([10, 13])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/10-13", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([11, 14])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/11-14", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([12, 15])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/12-15", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([5, 6])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/5-6", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([13, 16])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/13-16", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([14, 17])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/14-17", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([15, 18])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/15-18", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([6, 7])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/6-7", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([16, 19])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/16-19", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([17, 20])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/17-20", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([18, 21])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/18-21", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([7, 8])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/7-8", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([19, 22])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/19-22", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([20, 23])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/20-23", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([21, 24])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/21-24", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([8, 9])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/8-9", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([22, 25])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/22-25", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([23, 26])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/23-26", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([24, 27])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/24-27", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([9, 10])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/9-10", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([25, 28])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/25-28", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([26, 29])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/26-29", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([27, 30])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/27-30", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([10, 11])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/10-11", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([28, 31])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/28-31", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([29, 32])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/29-32", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([30, 33])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/30-33", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([11, 12])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/11-12", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([31, 34])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/31-34", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([32, 35])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/32-35", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([33, 36])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/33-36", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([12, 13])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/12-13", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([13, 14])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/13-14", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([14, 15])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/14-15", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([15, 16])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/15-16", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([16, 17])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/16-17", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([17, 18])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/17-18", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([18, 19])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/18-19", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([19, 20])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/19-20", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([20, 21])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/20-21", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([21, 22])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/21-22", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([22, 23])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/22-23", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([23, 24])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/23-24", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([24, 25])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/24-25", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([25, 26])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/25-26", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([26, 27])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/26-27", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([27, 28])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/27-28", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([28, 29])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/28-29", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([29, 30])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/29-30", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([30, 31])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/30-31", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([31, 32])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/31-32", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([32, 33])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/32-33", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([33, 34])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/33-34", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([34, 35])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/34-35", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([35, 36])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/35-36", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([0, 2])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/0-2", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([0, 3])(
    "Zaklad na dwa sasiadujace numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/split/0-3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );
});
