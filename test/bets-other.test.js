const { get, post } = require("../utils/http");

test("Player should have 100 chips", () => {
  return post("/players")
    .then((res) => get("/chips", res.hashname))
    .then((res) => expect(res.chips).toEqual(100));
});

[2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 29, 31, 33, 35].forEach(
  (number) => {
    test(`BLACK should double the number of chips if number ${number} is spinned`, () => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/black", hashname, { chips: 100 }); // a bet
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(200));
    });
  }
);

[1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36].forEach(
  (number) => {
    test(`RED should double the number of chips if number ${number} is spinned`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/red", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(200));
    });
  }
);

[1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35].forEach(
  (number) => {
    test(`Bet on ODDS should double the number of chips if number ${number} is spinned`, () => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/odd", hashname, { chips: 100 }); // a bet
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(200));
    });
  }
);

[0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36].forEach(
  (number) => {
    test(`Bet on EVEN should double the number of chips if number ${number} is spinned`, () => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/even", hashname, { chips: 100 }); // a bet
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(200));
    });
  }
);

[
  19,
  20,
  21,
  22,
  23,
  24,
  25,
  26,
  27,
  28,
  29,
  30,
  31,
  32,
  33,
  34,
  35,
  36,
].forEach((number) => {
  test(`Zaklad na wysokie liczby, liczba = ${number}`, () => {
    return post("/players")
      .then((response) => {
        hashname = response.hashname;
        return post("/bets/high", hashname, { chips: 100 }); // a bet
      })
      .then(() => post("/spin/" + number, hashname)) // splin the wheel
      .then(() => get("/chips", hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(200));
  });
});

[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18].forEach(
  (number) => {
    test(`Zakład na niskie numery, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/low", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(200));
    });
  }
);
