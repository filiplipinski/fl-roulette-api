const { get, post } = require("../utils/http");

describe("Pojedyncze numery", () => {
  test.each([1, 2, 3])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/1-2-3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([4, 5, 6])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/4-5-6", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([7, 8, 9])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/7-8-9", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([10, 11, 12])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/10-11-12", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([13, 14, 15])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/13-14-15", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([16, 17, 18])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/16-17-18", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([19, 20, 21])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/19-20-21", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([22, 23, 24])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/22-23-24", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([25, 26, 27])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/25-26-27", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([28, 29, 30])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/28-29-30", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([31, 32, 33])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/31-32-33", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([34, 35, 36])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/34-35-36", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([0, 1, 2])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/0-1-2", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );

  test.each([0, 2, 3])(
    "Zakład: 3 sąsiadujące kolejno numery. Numer = %d",
    (number) => {
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/street/0-2-3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname)) // splin the wheel
        .then(() => get("/chips", hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200));
    }
  );
});
