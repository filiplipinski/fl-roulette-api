const { get, post } = require("../utils/http");

describe("Kolumna numerow", () => {
  [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34].forEach((number) => {
    test(`Zakład na 1 kolumne numerów, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/column/1", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(300));
    });
  });

  [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35].forEach((number) => {
    test(`Zakład na 2 kolumne numerów, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/column/2", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(300));
    });
  });

  [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36].forEach((number) => {
    test(`Zakład na 2 kolumne numerów, liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/column/3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(300));
    });
  });
});
