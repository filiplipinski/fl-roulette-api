const { get, post } = require("../utils/http");

describe("Sasiadujace 4 numery", () => {
  [1, 2, 4, 5].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [1, 2, 4, 5], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/1-2-4-5", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [2, 3, 5, 6].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [2, 3, 5, 6], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/2-3-5-6", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [4, 5, 7, 8].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [4, 5, 7, 8], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/4-5-7-8", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [5, 6, 8, 9].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [5, 6, 8, 9], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/5-6-8-9", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [7, 8, 10, 11].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [7, 8, 10, 11], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/7-8-10-11", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [8, 9, 11, 12].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [8, 9, 11, 12], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/8-9-11-12", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [10, 11, 13, 14].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [10, 11, 13, 14], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/10-11-13-14", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [11, 12, 14, 15].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [11, 12, 14, 15], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/11-12-14-15", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [13, 14, 16, 17].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [13, 14, 16, 17], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/13-14-16-17", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [14, 15, 17, 18].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [14, 15, 17, 18], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/14-15-17-18", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [16, 17, 19, 20].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [16, 17, 19, 20], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/16-17-19-20", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [17, 18, 20, 21].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [17, 18, 20, 21], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/17-18-20-21", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [19, 20, 22, 23].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [19, 20, 22, 23], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/19-20-22-23", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [20, 21, 23, 24].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [20, 21, 23, 24], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/20-21-23-24", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [22, 23, 25, 26].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [22, 23, 25, 26], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/22-23-25-26", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [23, 24, 26, 27].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [23, 24, 26, 27], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/23-24-26-27", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [25, 26, 28, 29].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [25, 26, 28, 29], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/25-26-28-29", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [26, 27, 29, 30].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [26, 27, 29, 30], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/26-27-29-30", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [28, 29, 31, 32].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [28, 29, 31, 32], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/28-29-31-32", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [29, 30, 32, 33].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [29, 30, 32, 33], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/29-30-32-33", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [31, 32, 34, 35].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [31, 32, 34, 35], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/31-32-34-35", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [32, 33, 35, 36].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [32, 33, 35, 36], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/32-33-35-36", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });

  [0, 1, 2, 3].forEach((number) => {
    test(`Zakład na cztery sasiadujace numery [0, 1, 2, 3], liczba ${number}`, () => {
      let hashname = "";

      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/corner/0-1-2-3", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(900));
    });
  });
});
