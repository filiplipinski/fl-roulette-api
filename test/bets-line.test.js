const { get, post } = require("../utils/http");

describe("Linia numerow", () => {
  [1, 2, 3, 4, 5, 6].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [1,2,3,4,5,6], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/1-2-3-4-5-6", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [4, 5, 6, 7, 8, 9].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [4, 5, 6, 7, 8, 9], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/4-5-6-7-8-9", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [7, 8, 9, 10, 11, 12].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [7, 8, 9, 10, 11, 12], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/7-8-9-10-11-12", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [10, 11, 12, 13, 14, 15].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [10, 11, 12, 13, 14, 15], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/10-11-12-13-14-15", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [19, 20, 21, 22, 23, 24].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [19, 20, 21, 22, 23, 24], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/19-20-21-22-23-24", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [22, 23, 24, 25, 26, 27].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [22, 23, 24, 25, 26, 27], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/22-23-24-25-26-27", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [25, 26, 27, 28, 29, 30].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [25, 26, 27, 28, 29, 30], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/25-26-27-28-29-30", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [28, 29, 30, 31, 32, 33].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [28, 29, 30, 31, 32, 33], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/28-29-30-31-32-33", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });

  [31, 32, 33, 34, 35, 36].forEach((number) => {
    test(`Zakład na linie numerow, zwraca 6x zaklad, dla: [31, 32, 33, 34, 35, 36], liczba ${number}`, () => {
      let hashname = "";
      return post("/players")
        .then((response) => {
          hashname = response.hashname;
          return post("/bets/line/31-32-33-34-35-36", hashname, { chips: 100 });
        })
        .then(() => post("/spin/" + number, hashname))
        .then(() => get("/chips", hashname))
        .then((response) => expect(response.chips).toEqual(600));
    });
  });
});
